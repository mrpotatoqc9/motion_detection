"""
By: Patrick C-M
Date:  June 2020
Description:
Motion detection 
"""

import cv2
import numpy as np

first_frame = None
#kernel = np.ones((3,3),np.uint8)
cap = cv2.VideoCapture(0)
print("testing")
while True:
	
	check, frame = cap.read()
	
	gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
	gray=cv2.GaussianBlur(gray,(21,21),0)

	if first_frame is None:
	    	first_frame = gray


	framediff = cv2.absdiff(first_frame,gray)
	dst,threshdiff = cv2.threshold(framediff,30,255,cv2.THRESH_BINARY)
	#threshdiff = cv2.dilate(threshdiff,None,iterations=1)
	threshdiff = cv2.erode(threshdiff,None,iterations=2)
	contours,hierarchy = cv2.findContours(threshdiff.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)

	#frame = cv2.drawContours(frame, contours, -1, (0,255,0), 3)	
	for cnts in contours :
		if cv2.contourArea(cnts) < 2000:
			continue
		(x,y,w,h) = cv2.boundingRect(cnts)
		cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),3)
	
	cv2.imshow('thresh',threshdiff)
	cv2.imshow('frame',frame)
	#cv2.imshow('capturing',gray)
	#cv2.imshow('diff',framediff)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()

